# Use the official Python base image
FROM python:3.9-slim-buster

# Set the working directory inside the container
WORKDIR /app

# Upgrade pip version
RUN pip install --no-cache-dir --upgrade pip

# Copy project files into the container's working directory
COPY . /app

# Install Python dependencies
RUN pip install gunicorn
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 8080 for the Flask web server
EXPOSE 8080

# Start the microservice
CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:8080", "app:app"]
