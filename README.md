# Hello World Python Microservice

This is a simple open source learning project that demonstrates the implementation of a basic Hello World microservice using Python and Docker with Gunicorn.

## Overview

The "hello-world-python-microservice" project showcases the setup and deployment of a microservice architecture using Python as the programming language. It uses Docker for containerization and Gunicorn as the WSGI HTTP server.

## Features

- Accepts HTTP GET requests and responds with a "Hello, World!" message.
- Utilizes the Flask web framework to handle incoming requests.
- Implements Gunicorn as the HTTP server for running the microservice.
- Packaged as a Docker image for easy deployment and isolation.

## Prerequisites

To run this project locally, you need to have the following prerequisites installed:

- Docker: [Install Docker](https://docs.docker.com/get-docker/)

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository:

```bash
git clone <repository-url>
```

2. Navigate to the project directory:

```bash
cd hello-world-python-microservice
```

3. Build the Docker image:

```bash
docker build -t hello-world-python-microservice .
```

4. Run the Docker container:

```bash
docker run -p 8000:8000 hello-world-python-microservice
```

5. Access the microservice:

Open your web browser and visit [http://localhost:8000](http://localhost:8000). You should see the "Hello, World!" message.

## Contributing

This project is open for contributions. If you have any ideas, improvements, or bug fixes, please feel free to submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

- [Flask](https://flask.palletsprojects.com/) - Web framework used for handling HTTP requests.
- [Gunicorn](https://gunicorn.org/) - WSGI HTTP server for running the microservice.
- [Docker](https://www.docker.com/) - Containerization platform used for packaging and deploying the microservice.

## Contact

For any questions or feedback regarding this project, please reach out to [your-email@example.com](mailto:boris@crismancich.com).
