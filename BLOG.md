# Hello world - My first (hopefully well done) microservice architecture

## 2023 July 10 - Day 2

- Created this project documentation

I'm happy with the result. And quite proud that I finally did for the first time, what I asked my dev teams to do so many times. Feels like walking in their shoes.

## 2023 July 9 - Day 1

- Done that: Create GitLab Repository
- Already there: Visual Studio Code
- Installed docker on my local machine (temporary for testing)
- ChatGPT did that: Hello world is hard work. Program Python Microservice
- Used Flask web server
- Learned how to Docker file
- Set up Gunicorn web server interface
- Started the container. It works!

I'm happy with the result. And quite proud that I finally did for the first time, what I asked my dev teams to do so many times. Feels like walking in their shoes.

## The beginning

Why am I learning to code again?

Because I miss it. About 7 years ago I stopped being a developer and slowly transformed my job towards technical business development. So I saw my colleagues using technologies like containerization and I planned it in. But I never used it. It's not my job to program real stuff any more. But it's a wish. So as a side project, I'm learning how to create a proper microservice. With a proper build pipeline and all. Every day I want to make a little puzzle piece.

### Goal

I want to create a simple hello world microservice architecture in python.

### Plan

- Create GitLab Repository
- Set up Visual Studio Code
- Program Python Microservice
  - Flask web server
- Docker Container
  - Set up Gunicorn web server interface
- Add docker to GitLab Container Registry
- Create Linux VM (target Server)
- Install Kubernetes Cluster
- Install Nginx Ingress Controller

This project involves developing and deploying a Python microservice using GitLab, Docker, and Kubernetes. The microservice will be a simple "Hello world" application built with Flask, a lightweight web framework. The goal is to containerize the microservice using Docker, set up a GitLab repository to store the code and Docker image, and deploy the microservice in a Kubernetes cluster.

To facilitate the deployment, the project will utilize Visual Studio Code as the development environment for writing and testing the microservice code. Gunicorn, a Python WSGI HTTP server, will be integrated into the Docker container to handle incoming requests. The Docker image will be stored in the GitLab Container Registry for easy access.

In order to deploy the microservice, a Linux virtual machine will be created to serve as the target server for the Kubernetes cluster. The Kubernetes cluster will be installed on this virtual machine, and the Nginx Ingress Controller will be set up within the cluster to act as a reverse proxy and load balancer.

Overall, this project aims to demonstrate the end-to-end process of developing a Python microservice, containerizing it with Docker, utilizing GitLab for code and image management, and deploying the microservice in a Kubernetes cluster with the help of the Nginx Ingress Controller.
